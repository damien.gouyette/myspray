package com.example.metrics

import com.yammer.metrics.Metrics
import com.yammer.metrics.scala.{Counter, Timer}
import com.example.api.{CustomerManager, RoomAvail, RoomManager, HotelManager}

/**
 * Created with IntelliJ IDEA.
 * User: gbougeard
 * Date: 19/06/13
 * Time: 12:04
 * To change this template use File | Settings | File Templates.
 */
object MyMetrics {
  val metricCustomerManager = Metrics.defaultRegistry().newCounter(classOf[CustomerManager], "count")
  val countCustomerManager = new Counter(metricCustomerManager)

 val metricHotelManager = Metrics.defaultRegistry().newCounter(classOf[HotelManager], "count")
  val countHotelManager = new Counter(metricHotelManager)

  val metricRoomManager = Metrics.defaultRegistry().newCounter(classOf[RoomManager], "count")
  val countRoomManager = new Counter(metricRoomManager)

  val metricRoomAvail= Metrics.defaultRegistry().newCounter(classOf[RoomAvail], "count")
  val countRoomAvail= new Counter(metricRoomAvail)
}
