package com.example.api

import akka.actor.{Props, PoisonPill, Actor, ActorLogging}
import akka.event.LoggingReceive
import com.example.metrics.MyMetrics

/**
 * Created by gbougeard on 12/06/13.
 */
object RoomAvail{
  def apply(name:String): Props =
    Props(classOf[RoomAvail],name)
}


class RoomAvail(name:String) extends Actor with ActorLogging{
  import BookingProtocol._

  var avails = Vector[Avail]()
  val date = name

  override def preStart() = MyMetrics.countRoomAvail += 1

  def receive = LoggingReceive{

    case GetAvails(hotel) => sender ! avails.size

    case Avails(newAvails) => avails = avails ++ newAvails

    case BookRoom =>
      if (avails.isEmpty) {
        sender ! SoldOut
        self ! PoisonPill
      }

      avails.headOption.foreach { avail =>
        avails = avails.tail
        sender ! avail
      }

    case _ => log.warning("unhandled msg")

  }
}
