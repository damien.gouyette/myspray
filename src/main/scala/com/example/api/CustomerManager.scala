package com.example.api

import akka.actor._
import scala.Some
import scala.concurrent.Future
import scala.concurrent.duration._
import akka.util.Timeout
import akka.event.LoggingReceive
import com.example.domain.HotelCatInvs
import com.example.metrics.MyMetrics
import org.joda.time.DateTime


class CustomerManager extends Actor with CreateHotel with ActorLogging {

  import BookingProtocol._
  import context._

  implicit val timeout = Timeout(5 seconds)


  import com.yammer.metrics.Metrics
  import com.yammer.metrics.scala.Timer

  val metricInitAvail = Metrics.defaultRegistry().newTimer(classOf[CustomerManager], "initAvail")
  val timerInitAvail = new Timer(metricInitAvail)
  val metricSetAvail = Metrics.defaultRegistry().newTimer(classOf[CustomerManager], "setAvail")
  val timerSetAvail = new Timer(metricSetAvail)
  val metricGetAvails = Metrics.defaultRegistry().newTimer(classOf[CustomerManager], "getAvails")
  val timerGetAvails = new Timer(metricGetAvails)
  val metricGetRoomAvails = Metrics.defaultRegistry().newTimer(classOf[CustomerManager], "getRoomAvails")
  val timerGetRoomAvails = new Timer(metricGetRoomAvails)
  val metricBookingRequest = Metrics.defaultRegistry().newTimer(classOf[CustomerManager], "BookingRequest")
  val timerBookingRequest = new Timer(metricBookingRequest)

  override def preStart() = MyMetrics.countCustomerManager += 1


  def receive = LoggingReceive {

//    case Avail(room, date, qty) =>
//      log.info(s"Adding avail for room ${room} on date ${date} qty: ${qty}.")
//      timerSetAvail.time(setAvail(room, date, qty))

//    case BookingRequest(room, date) =>
//      log.info(s"Getting a room ${room} for the date ${date} .")
//
//      context.child(room) match {
//        case Some(roomAvail) => roomAvail.forward(BookRoom)
//        case None => sender ! SoldOut
//      }
//
//    case GetRoomAvails(room) =>
//      timerGetRoomAvails.time(getRoomAvail(room))

    case GetAvails(hotelId) =>
     timerGetAvails.time(getAvails(hotelId))

//    case GetHotels =>
//      val hotels = HotelList(domain.Hotels.findAll.map {
//        h =>
//          BookingProtocol.Hotels(h.id, h.name, h.city)
//      })
//      sender ! hotels

    case InitHotels(custid:Long) =>
      timerInitAvail.time(initHotels(custid))
      sender ! InitDone

    case InitRandomHotels(nbHotels:Long) =>
      timerInitAvail.time(initRandomHotels(nbHotels))
      sender ! InitDone
  }

  private def getAvails(hotelid:Long) = {
    import akka.pattern.ask
    val capturedSender = sender

    def askAndMapToEvent(roomAvail: ActorRef) = {
      log.debug(s"askAndMapToEvent ${roomAvail.actorRef.path.name}")
      roomAvail.ask(GetAvails).mapTo[Avails]
      //        log.debug(s"self ${self.path.name} child ${roomAvail.actorRef.path.name}")
      //        futureInt.map(avails => Avail(self.path.name, roomAvail.actorRef.path.name, avails))
      //future.map(avails => avails) //Avail(room, ticketSeller.actorRef.path.name, nrOfTickets))
    }

    val futures = context.children.map(roomAvail => askAndMapToEvent(roomAvail))

    Future.sequence(futures).map {
      events =>
        val avails = events.map {
          avails => avails.avails
        }
        capturedSender ! Avails(avails.flatten.toList)
    }
  }

  private def getRoomAvail(room: String) = {
    import akka.pattern.ask
    import akka.pattern.pipe

    val capturedSender = sender
    log.debug(s"GetRoomAvails $room")

    context.child(room) match {
      case Some(roomManager) => {
        val future: Future[Avails] = ask(roomManager, GetAvails).mapTo[Avails]
        future pipeTo capturedSender
        future.map {
          x => log.debug(s"$x")
        }
      }
      case None => {
        capturedSender ! SoldOut
      }
    }
  }

  private def initHotels(id: Long) = {
    log.info(s"Init Hotel for custid $id .")
    HotelCatInvs.findByCustId(id).map {
      avail =>
        setAvail(avail.hotelid, avail.catCode.toString, avail.from.toString("YYYYMMdd"), avail.avail)
    }
    log.info(s"InitDone for Customer $id")

  }

  private def initRandomHotels(nb: Long) = {
    import scala.util.Random
    log.info(s"Init Random $nb Hotel .")
    for(id <- 1 to nb.toInt) {
        setAvail(Random.nextLong(), "random", new DateTime().toString("YYYYMMdd"), Random.nextInt())
    }
    log.info(s"$nb random hotels created")

  }

  private def setAvail(hotelid:Long, room: String, date: String, qty: Int) = {
    log.debug(s"setAvail $hotelid $room $date $qty")

    context.child(hotelid.toString) match {
      case Some(hotelManager) => {
        log.debug("send avails to hotelManager")
        hotelManager ! InitAvail(hotelid)
      }
      case None => {
        log.debug(s"create Room Manage for room $room")
        val hotelManager = createHotel(hotelid)
        hotelManager ! InitAvail(hotelid)
      }
    }

    sender ! AvailCreated
  }
}

trait CreateHotel {
  self: Actor =>
  def createHotel(name: Long) = context.actorOf(HotelManager(name), name.toString)
}
