package com.example.domain

import scala.slick.driver.MySQLDriver.simple._
import org.joda.time.DateTime
import com.github.tototoshi.slick.JodaSupport._

// Use the implicit threadLocalSession
//import Database.threadLocalSession

/**
 * Created by gbougeard on 6/3/13.
 */

case class HotelCatInv(hotelid: Long,
                       custid: Long,
                       groupId: Long,
                       from: DateTime,
                       to: DateTime,
                       catCode: Long,
                       roomtCode: Long,
                       saleStatus: String,
                       avail: Int)


// Definition of the SUPPLIERS table
object HotelCatInvs extends Table[HotelCatInv]("hotelcatinv") {
  def hotelId = column[Long]("hotelid")

  def custId = column[Long]("custid")

  def groupId = column[Long]("groupid")

  def from = column[DateTime]("fromdate")

  def to = column[DateTime]("todate")

  def catCode = column[Long]("catcode")

  def roomtCode = column[Long]("roomtcode")

  def saleStatus = column[String]("salestatus")

  def avail = column[Int]("availlimit")

  // Every table needs a * projection with the same type as the table's type parameter
  def * = hotelId ~ custId ~ groupId ~ from ~ to ~ catCode ~ roomtCode ~ saleStatus ~ avail <>(HotelCatInv, HotelCatInv.unapply _)

  val byHotelId = createFinderBy(_.hotelId)
  val byCustId = createFinderBy(_.custId)
  val byGroupId = createFinderBy(_.groupId)

  lazy val pageSize = 10

  val DB = MyDatabase.cnx


  import com.yammer.metrics.Metrics
  import com.yammer.metrics.scala.Timer

  val metric = Metrics.defaultRegistry().newTimer(classOf[Hotel], "findByHotelid")
  val timer = new Timer(metric)

  def findByHotelId(id: Long): List[HotelCatInv] = DB.withSession {
    implicit session => {
      HotelCatInvs.byHotelId(id).list
    }
  }

  def findByCustId(id: Long): List[HotelCatInv] = DB.withSession {
    implicit session => {
      HotelCatInvs.byCustId(id).list
    }
  }

  def findByGroupId(id: Long): List[HotelCatInv] = DB.withSession {
    implicit session => {
      HotelCatInvs.byGroupId(id).list
    }
  }

  def count: Int = DB.withSession {
    implicit session => {
      Query(HotelCatInvs.length).first
    }
  }

}