package com.example.domain


import scala.slick.driver.MySQLDriver.simple._
import scala.slick.session.Session
import scala.slick.session.Database
import spray.json.DefaultJsonProtocol
import com.example.api.BookingProtocol._

// Use the implicit threadLocalSession
//import Database.threadLocalSession

/**
 * Created by gbougeard on 6/3/13.
 */

case class Hotel(id: Long,
                 name: String,
                 city: String)


// Definition of the SUPPLIERS table
object Hotels extends Table[Hotel]("hotel") {
  def id = column[Long]("hotelid", O.PrimaryKey)

  // This is the primary key column
  def name = column[String]("name")

  def city = column[String]("city")

  // Every table needs a * projection with the same type as the table's type parameter
  def * = id ~ name ~ city <>(Hotel, Hotel.unapply _)

  val byId = createFinderBy(_.id)
  val byName = createFinderBy(_.name)
  val byCity = createFinderBy(_.city)

  lazy val pageSize = 1000

  val DB = MyDatabase.cnx

  def findAll: List[Hotel] = DB.withSession {
    implicit session => {
      (for (c <- Hotels.sortBy(_.name).take(pageSize)) yield c).list
    }
  }

  def findPage(page: Int = 0): Page[Hotel] = {

    val offset = pageSize * page

    DB.withSession {
      implicit session =>
        val clubs = (
          for {c <- Hotels
            .drop(offset)
            .take(pageSize)
          } yield c).list
        Page(clubs, page, offset, count)
    }
  }

  def findById(id: Long): Option[Hotel] = DB.withSession {
    implicit session => {
      Hotels.byId(id).firstOption
    }
  }

  def count: Int = DB.withSession {
    implicit session => {
      Query(Hotels.length).first
    }
  }

}