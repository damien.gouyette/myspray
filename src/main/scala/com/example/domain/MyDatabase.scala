package com.example.domain

import com.typesafe.config.{ConfigFactory, Config}
import scala.slick.session.Database

/**
 * Created with IntelliJ IDEA.
 * User: gbougeard
 * Date: 18/06/13
 * Time: 23:23
 * To change this template use File | Settings | File Templates.
 */
object MyDatabase {
    val config = ConfigFactory.load
    val url = config.getString("db.url")
    val user = config.getString("db.user")
    val password = config.getString("db.password")
    val driver = config.getString("db.driver")
    val cnx = Database.forURL(s"${url}?user=${user}&password=${password}", driver = "com.mysql.jdbc.Driver")

}
